import file_io
import argparse


def parse_args():
    """
    Parse the arguments
    :return: The parsed arguments
    """
    
    parser = argparse.ArgumentParser(description='Manages anime_list.json', add_help=True, version='1.0')
    parser.add_argument('-l', '--list', action='store_true', help='List all anime in file', default=False)
    
    args, sub_commands = parser.parse_known_args()
    
    if args.list:
        print args
        return args
    else:
        subparser = parser.add_subparsers(title='Actions', description='Actions that can be performed', dest='command')

        add = subparser.add_parser('=add', help='Add anime entry')
        add.add_argument('-n', '--name', type=str, required=False, default='',
                         help='The name of the anime adding. Will be auto filled in if left blank')
        add.add_argument('-e', '--episode', type=int, required=False, default=0,
                         help='The last watched episode. Download starts add +1 this episode')
        add.add_argument('-u', '--url', type=str, required=True, help='A link to any episode for the anime')

        remove = subparser.add_parser('=remove', help='Remove anime entry')
        remove.add_argument('-n', '--name', type=str, required=True, default='',
                            help='The name of the anime to remove')
        
        args = parser.parse_args()
        print args
    
    return args


def print_anime():
    """
    Prints a list of all the anime in the file
    :return: None
    """
    
    data = file_io.read_anime_list()['anime']
    
    for anime in data:
        print anime[0]
        
        
def add_entry(name, url, episode):
    """
    Adds an anime entry
    :param name: The name of the anime to add
    :param url: The URL to an episode of the anime
    :param episode: The last downloaded episode number
    :return: None
    """
    
    data = file_io.read_anime_list()
    data['anime'].append([name, url, episode])
    
    file_io.write_anime_list(data)
    
    
def remove_entry(name):
    """
    Remove an anime entry
    :param name: The name of the entry to remove. If more than one entry exists with the same name, the first entry with that name will be removed
    :return: None
    """

    data = file_io.read_anime_list()
    
    for i in xrange(0, len(data['anime'])):
        if data['anime'][i][0] == name:
            data['anime'].pop(i)
            break
            
    file_io.write_anime_list(data)
    
    
def main():
    """
    Main method
    :return: None
    """
    args = parse_args()
    
    if args.list:
        print_anime()
    elif args.command == '=add':
        add_entry(args.name, args.url, args.episode)
    elif args.command == '=remove':
        remove_entry(args.name)

if __name__ == '__main__':
    main()

