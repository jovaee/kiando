import os
import re
import time
import getpass

from Exceptions.ScrapperExceptions import *

# Selenium imports
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException, NoSuchElementException


class Scraper:
    """
    
    """
    
    def __init__(self, browser='firefox', driver_name='geckodriver', add_blocker=None, timeout=35, load_time=7.5):
        if not os.path.exists(os.getcwd() + '\\' + driver_name):
            print('ERROR: The specified driver cannot be found at %s' % (os.getcwd() + '\\' + driver_name))
            exit()
        
        self.current_index = 0
        self.start_index = 0
        self.end_index = 0
        
        self.url = 'http://kissanime.ru'
        
        self.browser = browser
        self.driver_name = driver_name
        
        self.add_blocker = add_blocker
        
        self.timeout = timeout
        self.load_time = load_time
        
        self._get_driver()
        
        self.driver.set_page_load_timeout(self.load_time)
    
    def _find_extension(self):
        """
        
        :return:
        """
        
        # print 'Username of user running script: %s' % getpass.getuser()
        
        # TODO: Add support for Chrome browser extensions
        if self.browser == 'firefox':
            base_path = "C:/Users/%s/AppData/Roaming/Mozilla/FireFox/Profiles/" % getpass.getuser()
            
            base_path = base_path + os.listdir(base_path)[0] + '/extensions/'
            
            for name in os.listdir(base_path):
                if self.add_blocker.lower() in name.lower():
                    # print 'Adblocker "%s" found at: %s' % (self.add_blocker, base_path + '' + name)
                    print 'Adblocker "%s" found' % self.add_blocker
                    return base_path + '' + name
            
            print 'No adblocker named "%s" found' % self.add_blocker
    
    def _get_driver(self):
        """
        
        :return:
        """
        
        if self.browser == 'firefox':
            if self.add_blocker is not None:
                profile = webdriver.FirefoxProfile()
                profile.add_extension(self._find_extension())
                
                self.driver = webdriver.Firefox(profile, executable_path=self.driver_name)
            else:
                self.driver = webdriver.Firefox(executable_path=self.driver_name)
        
        elif self.browser == 'chrome':
            if self.add_blocker is not None:
                # profile = webdriver.ChromeOptions()
                # profile.add_extension(self._find_extension())
                
                self.driver = webdriver.Chrome(executable_path=self.driver_name)
                # self.driver = webdriver.Chrome(chrome_options=profile, executable_path=self.driver_name)
            else:
                self.driver = webdriver.Chrome(executable_path=self.driver_name)
        
        else:
            print('ERROR: This browser is not yet supported')
            self.quit()
            exit()
    
    def set_base_url(self, url):
        """
        
        :param url:
        :return: None
        """
        
        self.url = url
    
    def go_to_base_url(self):
        """

        :return: None
        """
        
        self.driver.get(self.url)
    
    def get_anime_name(self):
        """
        
        :return: The anime's name
        """
        
        elem = WebDriverWait(self.driver, self.timeout).until(ec.presence_of_element_located((By.ID, 'navsubbar')))
        
        text = elem.find_element_by_xpath('//div[@id="navsubbar"]//p//a').get_property('text')
        
        texts = re.split('\\s+', text)
        
        name = ' '.join(texts[2:-1])
        
        print 'Anime name: %s' % name
        return name
    
    def _go_to_correct_page(self, index):
        """
        Goes to the episode page as defined by 'current_index'
        :param index:
        :return: True of the switch could be made else false
        """
        
        elem = WebDriverWait(self.driver, self.timeout).until(ec.presence_of_element_located((By.ID, 'selectEpisode')))
        
        try:
            Select(elem).select_by_visible_text('Episode %s' % str(index).zfill(3))
            time.sleep(self.load_time)
            
            return True
        except Exception:
            return False
    
    def get_links(self, quality='360p'):
        """
        
        :return: The links of all the videos in the specified range and the last retrieved anime episode number
        """
        
        # If we are not on the correct episode page, go to the correct one
        if self._get_episode_num() != self.start_index:
            if not self._go_to_correct_page(self.start_index):
                # The switch was not possible since an entry does not exist in the selector on the web page
                raise EpisodeNotFoundException('The required episode, Episode %d, does not exist. Cannot switch to it' % self.start_index)
        
        tmp = self.current_index
        links = []
        
        for i in xrange(tmp, self.end_index + 1):
            links.append(self.get_next_link(quality=quality))
            
            if i != self.end_index and not self.get_next_page():
                return links, i
        
        return links, self.end_index
    
    def _get_episode_num(self):
        """

        :return:
        """
        
        elem = WebDriverWait(self.driver, self.timeout).until(ec.presence_of_element_located((By.ID, 'selectEpisode')))
        
        while True:
            try:
                text = Select(elem).first_selected_option.text
                
                return int(re.split(u'\\s+', text)[1])
            except NoSuchElementException:
                print 'Oh well'
    
    def _go_to_page(self, url):
        """
        
        :param url:
        :return:
        """
        
        self.driver.set_page_load_timeout(self.load_time)
        
        try:
            self.driver.get(url)
        except Exception:
            TimeoutException("I'm done waiting for this page to load. Just continue with the rest...")
    
    def set_base_url_to_page_url(self):
        """

        :return:
        """
        
        self.url = self.driver.current_url
    
    def get_next_page(self):
        """
        Goes to the next video page if it is in range and a link exists in the webpage.
        :return: True if the switch was possible otherwise false
        """
        
        WebDriverWait(self.driver, self.timeout).until(ec.presence_of_element_located((By.XPATH, '//div[@class="barContent"]')))

        try:
            right = self.driver.find_element_by_xpath('//img[@id="btnNext"]')
            
            print "Next button has been found, going there"
            self._go_to_page(right.find_element_by_xpath('..').get_property('href'))
            
            return True
        except Exception:
            print "Next button does not exist, stopping at %d" % (self.current_index - 1)
            return False

    def get_previous_page(self):
        """
        Goes to the previous video page if it is in range and a link exists in the webpage.
        :return: True if the switch was possible otherwise false
        """
        
        WebDriverWait(self.driver, self.timeout).until(ec.presence_of_element_located((By.XPATH, '//div[@class="barContent"]')))
        
        try:
            left = self.driver.find_element_by_xpath('//img[@id="btnPrevious"]')
            self.driver.get(left.find_element_by_xpath('..').get_property('href'))
            
            self.url = self.driver.current_url
            
            return True
        except Exception:
            print "Back button does not exist"
            return False

    def get_next_link(self, quality='360p'):
        """
        Get the URL to the video on the current page
        :return: The URL to the video or None for no URL
        """
        
        # If we are not on the correct episode page, go to the correct one
        if self._get_episode_num() != self.current_index:
            if not self._go_to_correct_page(self.current_index):
                # The switch was not possible since an entry does not exist in the selector on the web page
                raise EpisodeNotFoundException('The required episode, Episode %d, does not exist. Cannot switch to it' % self.current_index)
        
        # Check that we have not gotten all the required links
        if self.current_index > self.end_index:
            return None
        
        elem = WebDriverWait(self.driver, self.timeout).until(ec.presence_of_element_located((By.ID, 'selectQuality')))
        Select(elem).select_by_visible_text(quality)
        
        elem = self.driver.find_element_by_class_name('vjs-tech')
        
        self.current_index += 1
        
        return elem.get_property('src')
    
    def set_range(self, start_index, end_index):
        """
        
        :param start_index:
        :param end_index:
        :return:
        """
        
        self.current_index = start_index
        self.start_index = start_index
        self.end_index = end_index
    
    def clean_window(self):
        """
        Creates a new browser window and closes the old one
        :return: None
        """
        
        # Create new window
        self.driver.execute_script("(window.open())")
        
        # Switch to old window and close it
        self.driver.switch_to.window(self.driver.window_handles[0])
        self.driver.close()
        
        # Switch to the newly created window
        self.driver.switch_to.window(self.driver.window_handles[0])
    
    def quit(self):
        """
        Closes all open pages and quits web driver
        :return: None
        """
        
        try:
            self.driver.close()
            self.driver.stop_client()
            self.driver.quit()
        except Exception as e:
            print e.message
