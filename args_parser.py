import argparse


def parse_args():
    """
    Analyze the program arguments
    :return: Object containing all the arguments
    """

    class _HelpAction(argparse._HelpAction):
        """
        Custom argparse help handler
        Shows help for all subparsers as well
        """
    
        def __call__(self, parser, namespace, values, option_string=None):
            parser.print_help()
        
            # retrieve subparsers from parser
            subparsers_actions = [
                action for action in parser._actions
                if isinstance(action, argparse._SubParsersAction)]
            # there will probably only be one subparser_action,
            # but better save than sorry
            for subparsers_action in subparsers_actions:
                # get all subparsers and print help
                for choice, subparser in subparsers_action.choices.items():
                    print("Subparser '{}'".format(choice))
                    print(subparser.format_help())
        
            parser.exit()
    
    parser = argparse.ArgumentParser(description='Download anime from KissAnime', add_help=False, version='0.6')
    
    parser.add_argument('-h', '--help', action=_HelpAction, help='display help and exit')
    parser.add_argument('-br', '--browser', choices=['firefox', 'chrome'], type=str, default='firefox', required=False,
                        help='The name of the browser that will be used - NOTE: The correct driver should be used '
                             'with the chosen browser')
    parser.add_argument('-d', '--driver_name', default='geckodriver.exe', type=str, required=False,
                        help='The name of the driver to use - NOTE: The correct driver should be used with the chosen browser')
    parser.add_argument('-bl', '--blocker', choices=['ublock', 'adblock'], default=None, required=False,
                        help='The name of the ad blocker to use')
    parser.add_argument('-t', '--timeout', type=float, default=35, required=False,
                        help='The amount of time the scraper should wait for an element to appear')
    parser.add_argument('-q', '--quality', type=str, choices=['360p', '480p', '720p', '1080p'], required=False,
                        default='360p', help='The quality of the video that should be selected')
    parser.add_argument('-l', '--load_time', type=float, default=7.5, required=False,
                        help='How long the browser should wait for a page to load before starting web page scraping')
        
    subparser = parser.add_subparsers(title='Actions', description='Actions that can be performed', dest='command')

    # Fetch subparser
    fetch = subparser.add_parser('=fetch', help='Download anime episodes as described in anime_list.json')
    group = fetch.add_mutually_exclusive_group(required=True)
    group.add_argument('-st', '--steps', action='store_true',
                       help='Download episodes in steps. First get a link and then download the episode before proceeding')
    group.add_argument('-ba', '--batch', action='store_true', help='Get all links and then proceed to download the videos')
    
    # Normal get subparser
    get = subparser.add_parser('=get', help='Get links to anime video and download them or write links to file')
    get.add_argument('-s', '--start', type=int, required=True, help='The episode to start at')
    get.add_argument('-e', '--end', type=int, required=True, help='The episode to end at')
    get.add_argument('-u', '--url', type=str, required=True,
                        help='A link to any page that contains one of the episodes. The browser will auto navigate to the correct range before starting web page scraping')
    
    group = get.add_mutually_exclusive_group(required=True)
    group.add_argument('-st', '--steps', action='store_true',
                       help='Download episodes in steps. First get a link and then download the episode before proceeding')
    group.add_argument('-ba', '--batch', action='store_true', help='Get all links and then proceed to download the videos')
    group.add_argument('-wr', '--write', action='store_true', help='Write all links to a file')

    args = parser.parse_args()

    print 'Current script configuration:\n%s\n%s\n' % (args, '-' * (len(str(args)) / 2))
    
    return args
