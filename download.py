import sys
import util

import urllib
import urllib2

from Exceptions.DownloadExceptions import *
from tqdm import tqdm


def download(start, name, links):
    """
    Downloads all videos given by `links`. They are saved with filename `name` and episode counter starts at `start`.
    :param start: The start counter for numbering the video episodes
    :param name: The name to save the files with
    :param links: The video links to download
    :return: None
    """

    i = start
    for link in links:
        download_video(link, name + ' Episode ' + str(i))
        i += 1


def _download_progress_hook(t):
    """
    Wraps tqdm instance. Don't forget to close() or __exit__()
    the tqdm instance once you're done with it (easiest using `with` syntax).
    Example
    -------
    """
    last_b = [0]

    def inner(b=1, bsize=1, tsize=None):
        """
        b  : int, optional
            Number of blocks just transferred [default: 1].
        bsize  : int, optional
            Size of each block (in tqdm units) [default: 1].
        tsize  : int, optional
            Total size (in tqdm units). If [default: None] remains unchanged.
        """
        if tsize is not None:
            t.total = tsize
        t.update((b - last_b[0]) * bsize)
        last_b[0] = b

    return inner


def download_video(url, filename):
    """
    Download the video specified by url and save it to a file named filename
    :param url: The video URL
    :param filename: The name to save the file to
    :return: None
    """

    # save_folder = util.get_script_path() + '/VD/'
    # util.check_folder(save_folder)

    response = urllib2.urlopen(url)

    # print response.info()
        
    # Check that the video may not have already been downloaded
    if util.file_check('.'.join([filename, response.info()['Content-Type'].split('/')[1]]), int(response.info()['Content-Length'])):
        print '%s has already been downloaded' % '.'.join([filename, response.info()['Content-Type'].split('/')[1]])
        return
    
    # Create the filename with a .part file extension. This way non-complete and completed downloads can easily be differentiated
    filename = util.replace_invalid_win_chars('.'.join([filename, 'part']))

    print 'Downloading %s...' % (' '.join(filename.split('.')[:-1]))

    try:
        # Download the episode
        with tqdm(unit='B', unit_scale=True, leave=True, miniters=1) as t:
            urllib.urlretrieve(url, filename=filename, reporthook=_download_progress_hook(t))
    except Exception as e:
        # print 'An error occurred while downloading the video'
        print e.message
        
        raise DownloadingException('%s : Error while downloading video' % filename, filename)
    
    # Check that the file has downloaded correctly
    if util.file_check(filename, int(response.info()['Content-Length'])):
        # Rename file with correct extension
        new_name = '.'.join([filename.split('.')[0], response.info()['Content-Type'].split('/')[1]])
        util.rename_file(filename, new_name)
    else:
        # print 'The file did not download correctly: The file does not exist or the file size is not correct'
        raise DownloadedException('Downloaded file size does not match that of server file size' % filename, filename)


# Old progress indicator. Not used anymore
def progress_indicator(blocknum, blocksize, totalsize):
    """

    :param blocknum:
    :param blocksize:
    :param totalsize:
    :return: None
    """

    readsofar = blocknum * blocksize
    if totalsize > 0:
        percent = readsofar * 1e2 / totalsize
        s = "\r%5.1f%% %*d / %d" % (
            percent, len(str(totalsize)), readsofar, totalsize)
        sys.stderr.write(s)
        if readsofar >= totalsize:  # near the end
            sys.stderr.write("\n")
    else:  # total size is unknown
        sys.stderr.write("read %d\n" % (readsofar,))
