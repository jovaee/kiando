import re
import os


def replace_invalid_win_chars(string):
    """
    Removes any invalid characters from the string that Windows does not support in filenames
    :param string: The string to replace characters in
    :return: The cleaned up string
    """

    string = re.sub('<', '', string)
    string = re.sub('>', '', string)
    string = re.sub(':', '', string)
    string = re.sub('"', '', string)
    string = re.sub('/', '', string)
    string = re.sub('\\\\', '', string)
    string = re.sub('|', '', string)
    string = re.sub('\?', '', string)
    string = re.sub('\*', '', string)

    return string


def get_script_path():
    """
    Get the path to the folder where the this file is located in
    :return:
    """

    import sys
    
    return sys.path[0]


def check_folder(folder_name):
    """
    Check if the folder exists, if not create it
    :param folder_name: The name of the folder to check
    :return: None
    """

    if not os.path.exists(folder_name):
        os.mkdir(folder_name)


def delete_file(filename):
    """
    The the specified file
    :param filename: The name of the file to delete
    :return: None
    """

    if os.path.isfile(filename):
        os.remove(filename)
        

def rename_file(orig, new):
    """
    Rename a file
    :param orig: The original filname
    :param new: The new filename
    :return: None
    """
    
    if os.path.isfile(orig):
        os.rename(orig, new)
    else:
        print 'Could not rename file as the specified file does exists: %s' % orig
        
        
def file_check(filename, size):
    """
    Check if the specified exists and also check the file size
    :param filename: The name of the file
    :param size: The size the file must be
    :return: True of everything matches otherwise False
    """
    
    if os.path.isfile(filename) and int(os.stat(filename).st_size) == size:
        return True
    else:
        return False
