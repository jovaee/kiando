import download
import file_io
import util
import args_parser

from Exceptions.DownloadExceptions import *
from Exceptions import EpisodeNotFoundException

from scraper import Scraper


def fetch_new(args):
    """
    
    :return: None
    """
    
    def batch_fetch():
        """
        
        :return:
        """
    
        pass
    
    def single_fetch():
        """
        Get a single episode and download it
        :return: True if final found episode otherwise False
        """

        # Open the URL provided by the user
        scrap.go_to_base_url()

        link = scrap.get_next_link(args.quality)
        if not link:
            print "Have already gotten all links in range"
       
        if not scrap.get_next_page():
            print 'Downloading final found episode'

            scrap.clean_window()
            download.download(index, name, [link])
            
            return True
        else:
            print 'Downloading normally'

            scrap.set_base_url_to_page_url()
            scrap.clean_window()
    
            download.download(index, name, [link])
    
            return False
    
    # ------------------------------------
    max = 20000
    
    anime_list = file_io.read_anime_list()
    anime = anime_list['anime']

    # Start a scraper instance with the arguments the user supplied
    scrap = Scraper(browser=args.browser, driver_name=args.driver_name, add_blocker=args.blocker,
                    timeout=args.timeout, load_time=args.load_time)
    
    for entry in anime:
        completed_index = -1
  
        # Set the URL and episode range
        scrap.set_base_url(entry[1])
        scrap.set_range(entry[2] + 1, max)

        # Open the URL provided by the user and get the anime name
        try:
            scrap.go_to_base_url()
            name = scrap.get_anime_name()
        except Exception as e:
            # If an exception was caught here, continue to the next anime entry
            print e.message
            continue
            
        try:
            if args.steps:
                for index in xrange(entry[2] + 1, max + 1):
                    if single_fetch():
                        completed_index = index
                        break
        
                    completed_index = index
            elif args.batch:
                batch_fetch()
                
        except DownloadingException as de:
            print de.message
            util.delete_file(de.filename)
        except DownloadedException as de:
            print de.message
            util.delete_file(de.filename)
        except EpisodeNotFoundException as enfe:
            print enfe.message
        except KeyboardInterrupt:
            print 'Script stopped with KeyboardInterrupt'
            # TODO: Try to make this prettier and work better
            if entry[0] == '':
                entry[0] = name

            if completed_index != -1 and entry[2] < completed_index:
                entry[2] = completed_index

            file_io.write_anime_list(anime_list)
            return
        except Exception as e:
            print e.message

        if entry[0] == '':
            entry[0] = name

        if completed_index != -1 and entry[2] < completed_index:
            entry[2] = completed_index
    
    scrap.quit()
    file_io.write_anime_list(anime_list)
    

def batch_get(args):
    """
    Get all links in range and download them in batch or write all the links to a file
    :param args: The arguments supplied by the user
    :return: The name of the anime
    """
    
    # Start a scraper instance with the arguments the user supplied
    scrap = Scraper(browser=args.browser, driver_name=args.driver_name, add_blocker=args.blocker,
                    timeout=args.timeout, load_time=args.load_time)
    
    # Set the URL the web browser will start at
    scrap.set_base_url(args.url)
    
    # Set the episode range for video links to be retrieved
    scrap.set_range(args.start, args.end)
    
    # Open the URL provided by the user
    scrap.go_to_base_url()
    
    # Get all the link requested
    links, index = scrap.get_links(quality=args.quality)
    
    # Get the anime name
    name = scrap.get_anime_name()
    
    scrap.quit()
    
    if args.batch:
        # Download the episodes with the built-in downloader
        print 'Downloading videos...'
        download.download(args.start, name, links)
    elif args.write:
        # Don't download the episodes, just write the retrieved links to a file
        print 'Writing links to file...'
        file_io.write_links_to_file(name, links, start=args.start, end=index)
    
    return name


def single_get(args):
    """

    :param args: The arguments supplied by the user
    :return: None
    """
    
    # Start a scraper instance with the arguments the user supplied
    scrap = Scraper(browser=args.browser, driver_name=args.driver_name, add_blocker=args.blocker,
                    timeout=args.timeout, load_time=args.load_time)
    
    # Set the URL the web browser will start at
    scrap.set_base_url(args.url)
    
    # Set the episode range for video links to be retrieved
    scrap.set_range(args.start, args.end)
    
    # Open the URL provided by the user
    scrap.go_to_base_url()
    
    # Get the anime name
    name = scrap.get_anime_name()

    try:
        for i in xrange(args.start, args.end + 1):
            link = scrap.get_next_link(args.quality)
            if not scrap.get_next_page():
                print 'Downloading final found episode'
                
                # There is no next page so download the video and close script
                scrap.quit()
                download.download(i, name, [link])

                break
            else:
                print 'Downloading normally'
                
                scrap.set_base_url_to_page_url()
                scrap.clean_window()
                
                download.download(i, name, [link])
                
                scrap.go_to_base_url()
    except DownloadedException as de:
        print de.message
        util.delete_file(de.filename)
    except DownloadingException as de:
        print de.message
        util.delete_file(de.filename)
    except KeyboardInterrupt:
        print 'Script has been asked to stop...'
    except Exception as e:
        print e.message
            
    print 'Quiting...'
    scrap.quit()
    

def main():
    """
    The main method
    :return: None
    """
    
    args = args_parser.parse_args()
    
    if args.command == 'fetch':
        fetch_new(args)
    else:
        if args.steps:
            # Download video as links are retrieved
            single_get(args)
        else:
            # Get all links and then proceed with downloading or writing to file
            batch_get(args)


if __name__ == '__main__':
    main()
