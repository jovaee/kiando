class DownloadingException(Exception):
    """
    An exception that shows a problem occurred during active download time
    """
    
    def __init__(self, message, filename):
        """
        
        :param filename:
        """

        self.message = message
        self.filename = filename
        

class DownloadedException(Exception):
    """
    An exception that shows a problem occurred after download
    """

    def __init__(self, message, filename):
        """

        :param filename:
        """

        self.message = message
        self.filename = filename
