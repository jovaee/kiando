import json
import util


def write_links_to_file(name, links, start=None, end=None):
    """
    Write the links given to a file
    :param name: The name of the anime that links' are being written
    :param start: The start episode number
    :param end: The end episode number
    :param links: The list of links to save
    :return: None
    """

    if not start or not end:
        filename = util.replace_invalid_win_chars('%s URLs.txt' % name)
    else:
        filename = util.replace_invalid_win_chars('%s [%d - %d] URLs.txt' % (name, start, end))

    with open(filename, mode='w') as writer:
        for link in links:
            writer.write(link + '\n')


def read_anime_list():
    """
    Reads the data from anime_list.json
    :return: The data from the file
    """
    
    with open('anime_list.json', mode='r') as f:
        data = json.load(f)
        
    return data
    
    
def write_anime_list(data):
    """
    Writes updated date to anime_list.json
    :param data: The updated data
    :return: None
    """

    with open('anime_list.json', mode='w') as f:
        json.dump(data, f, indent=2, sort_keys=True)
